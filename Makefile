include config.mk
TARGET=vs
TARGETS=src/*/*.o src/*/*/*.o
SUBTARGETS=core modules ui

# Target directories
DESTDIR:=
PREFIX:=$(DESTDIR)/usr
BINDIR:=$(PREFIX)/bin
SHAREDIR:=$(PREFIX)/share



.PHONY: core modules ui clean mrproper build

# Main rule
build: $(SUBTARGETS)
	@[ -d bin ] || mkdir bin
	@echo "[1;34m[linking][0;34m $(TARGET)[0m"
	@$(CC) $(CFLAGS) $(LDFLAGS) $(TARGETS) -o bin/$(TARGET)

bin/$(TARGET): build

# Building rules
core:
	@$(MAKE) -C src/core build

modules:
	@$(MAKE) -C src/modules build

ui:
	@$(MAKE) -C src/ui build



# Admin rules
mrproper: clean
	rm -f bin/$(TARGET)

clean:
	@$(MAKE) -C src/core clean
	@$(MAKE) -C src/modules clean
	@$(MAKE) -C src/ui clean

install: bin/$(TARGET)
	install -Dm755 bin/$(TARGET) $(BINDIR)/$(TARGET)
	install -Dm644 doc/man/vs.1 $(SHAREDIR)/man/man1/vs.1
	gzip -9 $(SHAREDIR)/man/man1/vs.1

uninstall:
	rm $(BINDIR)/$(TARGET) $(SHAREDIR)/man/man1/vs.1.gz
