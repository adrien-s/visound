/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <visound/modules/events.h>
#include <visound/core/config.h>

/**
 * Raise up oscilloscope's horizontal sensivity
 * @param myCfg The config
 */
void evt_osc_sensh_up(config_t * myCfg)
{
	int i;
	float *tmp_x, *tmp_y, *old_x, *old_y;

	myCfg->scope.SensH_abs++;
	if (myCfg->scope.SensH_abs == 10) {
		myCfg->scope.SensH_abs=1;
		myCfg->scope.SensH_pow++;
	}
	myCfg->scope.SensH = ((float) myCfg->scope.SensH_abs) * pow(10.f, (float) myCfg->scope.SensH_pow);
	if (myCfg->mode == SCOPEXY3D) {
		myCfg->xy3D.bufSize = (int) (((float) myCfg->rate) * myCfg->scope.SensH);
		myCfg->xy3D.Cz = XY3D_Z_MAX / ((float) myCfg->xy3D.bufSize);
		myCfg->xy3D.curIdx = 0;
		myCfg->xy3D.lastIdx = 1;

		tmp_x = malloc(sizeof(float) * myCfg->xy3D.bufSize);
		tmp_y = malloc(sizeof(float) * myCfg->xy3D.bufSize);

		for(i=0 ; i<myCfg->xy3D.bufSize ; i++) {
			tmp_x[i] = 0.f;
			tmp_y[i] = 0.f;
		}

		// Free old buffers atomically
		old_x = myCfg->xy3D.buffer.x;
		old_y = myCfg->xy3D.buffer.y;
		myCfg->xy3D.buffer.x = tmp_x;
		myCfg->xy3D.buffer.y = tmp_y;
		free(old_x);
		free(old_y);


	} else {
		myCfg->scope.CoefH = (((float) myCfg->scope.width) * 0.1f) / (((float) myCfg->rate) * myCfg->scope.SensH);
	}
	syslog(LOG_INFO, "osc sensivity : %1.0e s/div\n", myCfg->scope.SensH);

}

/**
 * Lowers oscilloscope's horizontal sensivity
 * @param myCfg The config
 */
void evt_osc_sensh_down(config_t * myCfg)
{
	int i;
	float *tmp_x, *tmp_y, *old_x, *old_y;

	myCfg->scope.SensH_abs--;
	if (myCfg->scope.SensH_abs == 0) {
		myCfg->scope.SensH_abs=9;
		myCfg->scope.SensH_pow--;
	}
	myCfg->scope.SensH = ((float) myCfg->scope.SensH_abs) * pow(10.f, (float) myCfg->scope.SensH_pow);
	if (myCfg->mode == SCOPEXY3D) {
		myCfg->xy3D.bufSize = (int) (((float) myCfg->rate) * myCfg->scope.SensH);
		myCfg->xy3D.Cz = XY3D_Z_MAX / ((float) myCfg->xy3D.bufSize);
		myCfg->xy3D.curIdx = 0;
		myCfg->xy3D.lastIdx = 1;

		tmp_x = malloc(sizeof(float) * myCfg->xy3D.bufSize);
		tmp_y = malloc(sizeof(float) * myCfg->xy3D.bufSize);

		for(i=0 ; i<myCfg->xy3D.bufSize ; i++) {
			tmp_x[i] = 0.f;
			tmp_y[i] = 0.f;
		}

		// Free old buffers atomically
		old_x = myCfg->xy3D.buffer.x;
		old_y = myCfg->xy3D.buffer.y;
		myCfg->xy3D.buffer.x = tmp_x;
		myCfg->xy3D.buffer.y = tmp_y;
		free(old_x);
		free(old_y);
	} else {
		myCfg->scope.CoefH = (((float) myCfg->scope.width) *  0.1f) / (((float) myCfg->rate) * myCfg->scope.SensH);
	}
	syslog(LOG_INFO, "osc sensivity : %1.16e s/div\n", myCfg->scope.SensH);
}

/**
 * Raise up the vertical sensivity
 * @param myCfg The config
 */
void evt_osc_sensv_up(config_t * myCfg)
{
	myCfg->scope.SensV *= 1.4142135627f;
	myCfg->scope.CoefV = (((float) myCfg->scope.height) * 0.125f) / myCfg->scope.SensV;
}

/**
 * Lowers the vertical sensivity
 * @param myCfg The config
 */
void evt_osc_sensv_down(config_t * myCfg)
{
	myCfg->scope.SensV /= 1.4142135627f;
	myCfg->scope.CoefV = (((float) myCfg->scope.height) * 0.125f) / myCfg->scope.SensV;
}

/**
 * Raise up ghosting effect
 * @param myCfg The config
 */
void evt_osc_ghosting_up(config_t * myCfg)
{
	if (myCfg->scope.phosphore < 255) {
		myCfg->scope.phosphore++;
	}
}

/**
 * Lowers ghosting effect
 * @param myCfg The config
 */
void evt_osc_ghosting_down(config_t * myCfg)
{
	if (myCfg->scope.phosphore>0) {
		myCfg->scope.phosphore--;
	}
}

/**
 * Switch between regular update and fill-wait
 * @param myCfg The config
 */
void evt_osc_switch_fill(config_t * myCfg)
{
	myCfg->scope.fillscreen = (myCfg->scope.fillscreen==1)?0:1;

}

/**
 * @brief evt_osc_avg Corrects the amplification level
 * @param myCfg the config
 * @param b the current buffer
 */
void evt_osc_avg(config_t * myCfg, input_t * in)
{
	myCfg->scope.SensV = evt_global_avg(in) * 4.f;
	myCfg->scope.CoefV = (((float) myCfg->scope.height) * 0.125f) / myCfg->scope.SensV;
}

/**
 * Raises the beam pitch
 * @param myCfg The config
 */
void evt_osc_pitch_up(config_t * myCfg)
{
	myCfg->scope.pitch *= 1.4142135627f;
}

/**
 * Lowers the beam pitch
 * @param myCfg The config
 */
void evt_osc_pitch_down(config_t * myCfg)
{
	myCfg->scope.pitch /= 1.4142135627f;
}

/**
 * Raise up scan ampli
 * @param myCfg The config
 */
void evt_scan_ampli_up(config_t * myCfg)
{
	myCfg->scan.ampli *= 1.414213562f;
}

/**
 * Lowers scan ampli
 * @param myCfg The config
 */
void evt_scan_ampli_down(config_t * myCfg)
{
	myCfg->scan.ampli /= 1.414213562f;
}

/**
 * Eat a column (for realignment purposes)
 * @param myCfg The config
 */
void evt_scan_eat_column(input_t * in)
{
	buffer_read_sample(in);
}

/**
 * Eat a line (for realignment purposes)
 * @param myCfg The config
 */
void evt_scan_eat_line(config_t * myCfg, input_t * in)
{
	unsigned int i;
	size_t nb_pixels;

	nb_pixels = myCfg->scan.subchannels * myCfg->scan.used.width;
	for(i=0;i<nb_pixels;i++)
		buffer_read_sample(in);
}

/**
 * @brief evt_scan_avg Corrects the amplification level
 * @param myCfg the config
 * @param b the current buffer
 */
void evt_scan_avg(config_t * myCfg, input_t * in)
{
	myCfg->scan.ampli = 0.25f / evt_global_avg(in);
}

/**
 * Toggle between paused and unpaused mode
 * @param myCfg The config
 */
void evt_pause_toggle(config_t * myCfg)
{
	myCfg->paused = (myCfg->paused!=0)? 0 : 1;
	syslog(LOG_INFO, "%s !\n", (myCfg->paused==0)? "Unpaused":"Paused");
}

/**
 * @brief evt_global_avg Computes the required amplification level
 * @param b The buffer to use
 * @return the ampli value (float)
 */
float evt_global_avg(input_t * in)
{
	float avg;
	avg = buffer_avgValue(in);
	return (avg>=0)?avg:-avg;
}

/**
 * @brief evt_toggle_save Toggle whether we save frames or not
 * @param myCfg the config to use
 */
void evt_toggle_save(config_t * myCfg)
{
	if (myCfg->saving == 0 && myCfg->outputDirName!=NULL) {
		myCfg->saving = 1;
		return;
	}

	if (myCfg->saving == 1)
		myCfg->saving = 0;
}

/* modules/events.c */
