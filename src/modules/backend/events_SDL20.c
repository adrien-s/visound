/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#ifdef SDL20

#include <visound/core/config.h>
#include <visound/modules/events.h>

/**
 * Event management
 * @author Adrien Sohier
 */

/**
 * Oscilloscope Event handling for SDL12 interface
 * @param myCfg The config
 * @return  0: ok
 *         -1:  must quit
 */
int evt_osc(config_t * myCfg, ui_t * data, input_t * in)
{
	while (ui_getEvent(data)) {
		switch (data->event.type) {
		case SDL_KEYDOWN:
			switch (data->event.key.keysym.sym) {
			case SDLK_KP_PLUS:
				evt_osc_sensh_up(myCfg);
				break;
			case SDLK_KP_MINUS:
				evt_osc_sensh_down(myCfg);
				break;
			case SDLK_KP_MULTIPLY:
				evt_osc_sensv_up(myCfg);
				break;
			case SDLK_KP_DIVIDE:
				evt_osc_sensv_down(myCfg);
				break;
			case SDLK_PAGEUP:
				evt_osc_ghosting_up(myCfg);
				data->bgcolor = myCfg->scope.phosphore<<24;
				break;
			case SDLK_PAGEDOWN:
				evt_osc_ghosting_down(myCfg);
				data->bgcolor = myCfg->scope.phosphore<<24;
				break;
			case SDLK_HOME:
				evt_osc_pitch_up(myCfg);
				break;
			case SDLK_END:
				evt_osc_pitch_down(myCfg);
				break;
			case SDLK_f:
				evt_osc_switch_fill(myCfg);
				break;
			case SDLK_SPACE:
				evt_pause_toggle(myCfg);
				break;
			case SDLK_a:
				evt_osc_avg(myCfg, in);
				break;
			case SDLK_s:
				evt_toggle_save(myCfg);
				break;
			default: break;
			}
			break;
		case SDL_QUIT:
			return -1;
		default: break;
		}
	}
	return 0;
}

/**
 * Scan event handling for SDL12 interface
 * @param myCfg The config
 * @return  0: ok
 *         -1: must quit
 */
int evt_scan(config_t * myCfg, ui_t * data, input_t * in)
{
	while (ui_getEvent(data)) {
		switch (data->event.type) {
		case SDL_KEYDOWN:
			switch (data->event.key.keysym.sym) {
			case SDLK_PAGEUP:
				evt_scan_ampli_up(myCfg);
				break;
			case SDLK_PAGEDOWN:
				evt_scan_ampli_down(myCfg);
				break;
			case SDLK_UP:
				evt_scan_eat_line(myCfg, in);
				break;
			case SDLK_LEFT:
				evt_scan_eat_column(in);
				break;
			case SDLK_SPACE:
				evt_pause_toggle(myCfg);
				break;
			case SDLK_a:
				evt_scan_avg(myCfg, in);
				break;
			case SDLK_s:
				evt_toggle_save(myCfg);
				break;
			default: break;
			}
			break;
		case SDL_QUIT:
			return -1;
		default: break;
		}
	}
	return 0;
}
#endif
