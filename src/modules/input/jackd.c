/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <visound/modules/input/jackd.h>
#include <visound/core/config.h>

/**
 * Initializes the structure with the function ptr
 * @param in the input structure
 */
void jackd_initBuf(input_t * in, config_t * myCfg)
{
	if (in != NULL) {
		in->buffer.fillRoutine = &jackd_fillBuffer;
		in->jackd.nb_ports = myCfg->channels;

		if (jackd_in_start(in)<0) {
			syslog(LOG_ERR, "Couldn't initialize jackd audio, shutting down…");
			exit(-1);
		}
		myCfg->rate = (unsigned int) jack_get_sample_rate(in->jackd.client);
	}
}

/**
 * Fills the buffer with data
 * @param in The input structure
 */
void jackd_fillBuffer(input_t * in)
{
	u_int32_t i;

	for(i=0 ; i<in->buffer.bufferSize;i++) {
		// When the waitbuffer is empty, wait till the callback detects it and fill it again
		if (in->jackd.waitbuffer_offset == in->jackd.waitbuffer_size)
		{
			while (in->jackd.waitbuffer_offset > 0)
				usleep(1000);
		}

		// Read current waitbuffer
		in->buffer.samples[i] = (float) in->jackd.waitbuffer[in->jackd.waitbuffer_offset];
		in->jackd.waitbuffer_offset++;

	}
	in->buffer.samplesToRead = in->buffer.bufferSize;
}

/**
 * JackD callback to retrieve the data
 * @param nframes Number of frames to get
 * @param arg Other jack args
 * @return 0 : ok, !=0: error
 */
int jackd_callback(jack_nframes_t nframes, void * arg)
{
	u_int32_t i, j, nb, singlebufsize;
	input_t * inp = (input_t*) arg;
	jack_default_audio_sample_t * in, * res;

	// A few useful values
	nb = inp->jackd.nb_ports;
	singlebufsize = inp->jackd.singlebuffer_size;
	res = malloc(sizeof(jack_default_audio_sample_t)*inp->jackd.waitbuffer_size);

	// Get the buffers for each input port
	for(i=0 ; i<nb ; i++) {
		if (inp->jackd.input_ports == NULL)
			return -1;
		in = jack_port_get_buffer(inp->jackd.input_ports[i], nframes);
		/**
		 * Interleaves the channels :
		 * if we have 2 ports: port 1 → put to 0, 2, 4…
		 *					   port 2 → put to 1, 3, 5…
		 */
		for(j=0;j<singlebufsize;j++) {
			res[j*nb + i] = in[j];
		}
	}

	// Wait till the waitbuffer is empty to fill it
	while (inp->jackd.waitbuffer_offset<inp->jackd.waitbuffer_size)
		usleep(1000);

	// Fill the waitbuffer
	memcpy(inp->jackd.waitbuffer, res, sizeof(jack_default_audio_sample_t)*inp->jackd.waitbuffer_size);
	inp->jackd.waitbuffer_offset = 0;
	free(res);

	return 0;
}

/**
 * Cleans everything at exit
 * @param in The input structure to clean
 */
void jackd_clean(input_t * in)
{
	// Clean waitbuffer
	in->jackd.waitbuffer_offset = in->jackd.waitbuffer_size;
	while (in->jackd.waitbuffer_offset>0)
		usleep(1000);
	free(in->jackd.waitbuffer);
	in->jackd.waitbuffer = NULL;

	// Clean input ports
	free(in->jackd.input_ports);
	in->jackd.input_ports = NULL;

	// Close the client if it exists
	if (in->jackd.client != NULL)
		jack_client_close(in->jackd.client);
	in->jackd.client = NULL;
}

/**
 * @brief Shutdown the jackd connection
 * @param arg The input struct
 */
void jackd_in_shutdown(void * arg)
{
	jackd_clean((input_t*) arg);
}

/**
 * @brief Restarts jackd connection in case we've been dropped
 * @param arg
 */
void jackd_in_restart(void * arg)
{
	jackd_in_shutdown(arg);
	syslog(LOG_WARNING, "We've been kicked by Jack Server, trying to restart client");
	sleep(1);
	jackd_in_start((input_t*) arg);
}

/**
 * @brief Starts our client
 * @param in The input structure
 * @return 0 on success, -1 if JackD connection can't be established
 */
int jackd_in_start(input_t * in)
{
	u_int32_t i;
	char str[256], str_client[256];
	if (in != NULL) {
		in->jackd.client = NULL;
		in->jackd.input_ports = NULL;
		in->jackd.waitbuffer = NULL;

		in->jackd.input_ports = malloc(sizeof(jack_port_t*) * in->jackd.nb_ports);

		sprintf(str_client, "visound_%d", getpid());

		// Init JackD client
		in->jackd.client = jack_client_new(str_client);
		jack_set_process_callback(in->jackd.client, jackd_callback, (void*) in);
		jack_on_shutdown(in->jackd.client, jackd_in_restart, (void*) in);

		// Register input ports
		for(i=0 ; i<in->jackd.nb_ports ; i++) {
			sprintf(str, "in_%d", i);
			in->jackd.input_ports[i] = jack_port_register(in->jackd.client, str, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
		}

		// And… the activation :D
		if (jack_activate(in->jackd.client) != 0) {
			syslog(LOG_ERR, "Can't activate Jack client");
			jackd_in_shutdown((void*) in);
			return -1;
		}

		// What size are the buffers ?
		in->jackd.singlebuffer_size = (u_int32_t)jack_get_buffer_size(in->jackd.client);
		in->jackd.waitbuffer_size = in->jackd.singlebuffer_size * in->jackd.nb_ports;
		// Allocates the waitbuffer
		in->jackd.waitbuffer = (float *) calloc(in->jackd.waitbuffer_size, sizeof(float));
		in->jackd.waitbuffer_offset = in->jackd.waitbuffer_size;	// Buffer is initially empty
	}
	return 0;
}
