/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#include <stdio.h>
#include <stdlib.h>

#include <visound/modules/input/global.h>

/**
 * Initializes a buffer structure and gives it back
 * @param samplesPerCycle quantity of samples to fit
 * @return The initialized structure
 */
input_t * buffer_init(u_int32_t samplesPerCycle)
{
	input_t * ret = (input_t*) malloc(sizeof(input_t));

	if (ret != NULL) {
			ret->buffer.fillRoutine = NULL;
			ret->buffer.samples = (float*) calloc(samplesPerCycle, sizeof(float));
			ret->buffer.samplesToRead = ret->buffer.bufferSize = samplesPerCycle;
	}

	return ret;
}

/**
 * Reads a sample and decrements counter
 * If no sample was readable, calls the fill function and then, read a sample
 * @param in the input structure
 * @return a sample
 */
float buffer_read_sample(input_t * in)
{
	float res = 0;
	if (in != NULL) {
			if (in->buffer.samplesToRead<=0)
				in->buffer.fillRoutine(in);

			if (in->buffer.samplesToRead>=0) {
					res = in->buffer.samples[in->buffer.bufferSize - in->buffer.samplesToRead];
			}

			if (in->buffer.samplesToRead>0)
				in->buffer.samplesToRead--;
		}

	return res;
}

/**
 * Cleans everything
 * @param in the input structure to unload
 */
void buffer_clean(input_t * in)
{
	if (in != NULL) {
			free(in->buffer.samples);
			in->buffer.samples = NULL;
		}
}

/**
 * @brief buffer_avgValue Computes the average value in the buffer (useful for M62)
 * @param in The input to use for computations
 * @return The average floating point value
 */
float buffer_avgValue(input_t * in)
{
	double res = 0.f;
	u_int32_t i;

	if (in == NULL)
		return 0;

	for(i=0 ; i<in->buffer.bufferSize ; i++) {
			res += (in->buffer.samples[i]>0)?in->buffer.samples[i]:-(in->buffer.samples[i]);
		}
	res /= (double) in->buffer.bufferSize;
	return (float) res;
}
