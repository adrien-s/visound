/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#include <stdio.h>
#include <stdlib.h>

#include <visound/modules/input/alsa.h>
#include <visound/core/config.h>

/**
 * Initializes the structure with the function ptr
 * @param in the input structure
 */
void alsa_initBuf(input_t * in, config_t * myCfg)
{
	if (in != NULL) {
		in->buffer.fillRoutine = &alsa_fillBuffer;
		snd_pcm_hw_params_alloca(&in->alsa.hwparams);
		in->alsa.stream = SND_PCM_STREAM_CAPTURE;
		in->alsa.pcm_name = myCfg->input.alsa.device;
		in->alsa.frames = (snd_pcm_uframes_t) (in->buffer.bufferSize / myCfg->channels);

		if (snd_pcm_open(&(in->alsa.handle), myCfg->input.alsa.device, in->alsa.stream, 0)<0) {
			syslog(LOG_ERR, "Couldn't open alsa input\n");
		}
		snd_pcm_hw_params_any(in->alsa.handle, in->alsa.hwparams);

		// Sets the device
		snd_pcm_hw_params_set_access(in->alsa.handle, in->alsa.hwparams, SND_PCM_ACCESS_RW_INTERLEAVED);
		snd_pcm_hw_params_set_format(in->alsa.handle, in->alsa.hwparams, SND_PCM_FORMAT_FLOAT);
		snd_pcm_hw_params_set_rate_near(in->alsa.handle, in->alsa.hwparams, &(myCfg->rate), 0);
		snd_pcm_hw_params_set_channels(in->alsa.handle, in->alsa.hwparams, myCfg->channels);
		snd_pcm_hw_params_set_periods(in->alsa.handle, in->alsa.hwparams, 1, 0);
		snd_pcm_hw_params_set_buffer_size(in->alsa.handle, in->alsa.hwparams, in->alsa.frames);

		if (snd_pcm_hw_params(in->alsa.handle, in->alsa.hwparams)<0) {
			syslog(LOG_ERR, "Couldn't set alsa params\n");
		}
	}
}

/**
 * Fills the buffer with data
 * @param in The input structure
 */
void alsa_fillBuffer(input_t * in)
{
	if ( in != NULL) {
		snd_pcm_readi(in->alsa.handle, in->buffer.samples, in->alsa.frames);
		in->buffer.samplesToRead = in->buffer.bufferSize;
	}
}

/**
 * Cleans everything at exit
 * @param in The input structure to clean
 */
void alsa_clean(input_t * in)
{
	if (in->alsa.handle!=NULL) {
		snd_pcm_drop(in->alsa.handle);
		snd_pcm_drain(in->alsa.handle);
	}
}
