/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#include <stdio.h>
#include <stdlib.h>

#include <visound/modules/input/stdin.h>

/**
 * Initializes the structure with the function ptr
 * @param b the bufStruct structure
 */
void stdin_initBuf(input_t * in)
{
	if (in != NULL)
		in->buffer.fillRoutine = &stdin_fillBuffer;
}

/**
 * Fills the buffer with data
 * @param b The buffer structure
 */
void stdin_fillBuffer(input_t * in)
{
	float input;
	size_t input_size = sizeof(input);

	u_int32_t i;

	if (in != NULL) {
		for(i=0 ; i<in->buffer.bufferSize ; i++)
			if (fread(&(in->buffer.samples[i]), input_size, 1, stdin) < 1)
				in->buffer.samples[i] = 0.f;
		in->buffer.samplesToRead = in->buffer.bufferSize;
	}
}
