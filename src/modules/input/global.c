/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#include <stdlib.h>

#include <visound/modules/input/global.h>
#include <visound/modules/input/stdin.h>
#include <visound/modules/input/file.h>
#include <visound/modules/input/alsa.h>
#include <visound/modules/input/jackd.h>

/**
 * @brief input_init Initializes the data structure for input
 * @param samplesPerCycle number of cycles per input = number of samples in the buffer to collect at once
 * @param backend the backend flag to use
 * @return the input structure if OK, null if something has gone wrong
 */
input_t * input_init(u_int32_t samplesPerCycle, config_t * myCfg)
{
	input_t * i = buffer_init(samplesPerCycle);
	i->file = NULL;
	i->alsa.handle = NULL;
	if (i == NULL) {
		free(i);
		i = NULL;
	}

	switch(myCfg->input.type) {
	case type_stdin:
		stdin_initBuf(i);
		break;
	case type_file:
		file_initBuf(i, myCfg);
		break;
	case type_alsa:
		alsa_initBuf(i, myCfg);
		break;
	case type_jackd:
		jackd_initBuf(i, myCfg);
		break;
	default:
		input_clean(i);
		i = NULL;
		break;
	}

	return i;
}

/**
 * Frees everything useless in the structure at the end
 * @param i the structure to clean
 */
void input_clean(input_t * i)
{
	if (i != NULL) {
		alsa_clean(i);
		file_clean(i);
		jackd_clean(i);
		buffer_clean(i);
		free(i);
		i = NULL;
	}
}
