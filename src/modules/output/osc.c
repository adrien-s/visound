/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#include <stdio.h>
#include <stdlib.h>

#include <visound/core/modules.h>
#include <visound/modules/events.h>
#include <visound/modules/input/global.h>
#include <visound/ui/global.h>

/**
 * Oscilloscope dual channel module
 * @author Adrien SOHIER
 */


/**
 * Run oscilloscope with multiple channels
 * @param myCfg The config storage structure
 */
void osc_run(config_t * myCfg)
{
	syslog(LOG_INFO, "Oscilloscope multi channel started.");
	float input, pitch;
	u_int32_t color_dot = 0xFF0FFF1F;

	unsigned int i, read_samples = 0, screen_samples;
	unsigned char cont = 1;
	unsigned int cur_chan = 0;
	unsigned int * x_old = malloc(sizeof(unsigned int)*myCfg->channels);
	unsigned int * y_old = malloc(sizeof(unsigned int)*myCfg->channels);

	ui_t * window;
	input_t * input_data;

	window = ui_init(myCfg->scope.width, myCfg->scope.height);
	window->width = myCfg->scope.weight;
	ui_set_bgcolor(window, myCfg->scope.phosphore<<24);

	myCfg->scope.x = 0.f;
	myCfg->scope.offset_v = ((float) myCfg->scope.height) / (1.f + ((float) myCfg->channels));
	syslog(LOG_INFO, "osc : %d channels ==> offset is %f", myCfg->channels, myCfg->scope.offset_v);
	myCfg->scope.refresh_count = ((float) myCfg->rate) / myCfg->scope.fps;

	myCfg->paused = 0;

	// Calculate Coefficients from vertical & horizontal sensivity
	myCfg->scope.CoefV = (((float) myCfg->scope.height)*0.125f)/myCfg->scope.SensV;
	myCfg->scope.CoefH = (((float) myCfg->scope.width)*0.1f)/(((float) myCfg->rate) * myCfg->scope.SensH);
	screen_samples = 1000/myCfg->scope.CoefH;
	syslog(LOG_INFO, "osc : steps : h=%f, v=%f, screen samples: %d", myCfg->scope.CoefH, myCfg->scope.CoefV, screen_samples);

	input_data = input_init(screen_samples * myCfg->channels, myCfg);
	// No input, cannot continue.
	if (input_data == NULL) {
		syslog(LOG_ERR, "osc : Input module can't be initialized, aborting");
		free(x_old);
		free(y_old);
		ui_free(window);
		return;
	}

	for(i=0;i<myCfg->channels;i++) {
		x_old[i] = 0.f;
		y_old[i] = myCfg->scope.offset_v*i;
	}

	unsigned char synced=1;
	float trigger_low = myCfg->scope.trigger.level - myCfg->scope.trigger.precision,	// Trigger calculation
	        trigger_high = myCfg->scope.trigger.level + myCfg->scope.trigger.precision, float_tmp;

	if (trigger_high<trigger_low) {
		float_tmp = trigger_high;
		trigger_high = trigger_low;
		trigger_low = float_tmp;
	}

	while (cont != 0) {
		input = buffer_read_sample(input_data);
		if (myCfg->paused == 0) {

			if ( myCfg->scope.x == 0.f && cur_chan == myCfg->scope.trigger.channel) {
				if (myCfg->scope.trigger.channel<0 || (input>=trigger_low && input<=trigger_high)) {
					synced = 1;
				} else {
					synced = 0;
				}
			}

			if (cur_chan == myCfg->channels-1) {
				read_samples++;
			}
			myCfg->scope.y = (myCfg->scope.offset_v*(1.f+((float) cur_chan))) - (input * myCfg->scope.CoefV);

			// Draw osc line(s)
			if (synced && read_samples<screen_samples) {
				pitch = 255.f * (myCfg->scope.pitch / pow(sqrt(pow(((float) x_old[cur_chan]) - myCfg->scope.x,2) + pow(((float) y_old[cur_chan]) - myCfg->scope.y,2)),2));
				pitch = (pitch>255.f)? 255.f : pitch; 
				ui_drawline(x_old[cur_chan], y_old[cur_chan], (unsigned int) myCfg->scope.x, (unsigned int) myCfg->scope.y, (color_dot&0xFFFFFF) + (((unsigned int) pitch)<<24), window);
			}
			x_old[cur_chan] = (unsigned int) myCfg->scope.x;
			y_old[cur_chan] = (unsigned int) myCfg->scope.y;

			// Channel sync
			if (cur_chan == myCfg->channels-1) {
				if (synced && read_samples<screen_samples) {
					myCfg->scope.x += myCfg->scope.CoefH;
				}

				// Fill screen or follow screen rate ?
				if (read_samples==myCfg->scope.refresh_count) {
					ui_refresh(myCfg, window);
					read_samples = 0;
				}

				// End of screen detection
				if (myCfg->scope.x > myCfg->scope.width) {
					myCfg->scope.x = 0.f;

					for(i=0;i<myCfg->channels;i++) {
						x_old[i] = 0.f;
					}
					if (myCfg->scope.fillscreen == 1) {
						ui_set_bgcolor(window, 255<<24);
						ui_refresh(myCfg, window);
						ui_set_bgcolor(window, myCfg->scope.phosphore<<24);
					}
				}
			}
		}

		if (evt_osc(myCfg, window, input_data)<0) {
			cont = 0;
		}
		if (myCfg->paused == 0) {
			cur_chan++;
			if (cur_chan == myCfg->channels) {
				cur_chan = 0;
			}
		}
	}
	free(x_old);
	free(y_old);

	input_clean(input_data);
	ui_free(window);
}
