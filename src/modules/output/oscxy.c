/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#include <stdio.h>
#include <stdlib.h>

#include <visound/core/modules.h>
#include <visound/modules/events.h>
#include <visound/modules/input/global.h>
#include <visound/ui/global.h>

/**
 * Oscilloscope XY module
 * @author Adrien SOHIER
 */


/**
 * Run oscilloscope with 2-way XY mode
 * @param myCfg The config storage structure
 */
void osc_xy_run(config_t * myCfg)
{
	syslog(LOG_INFO, "Oscilloscope XY started.");
	float input_x, input_y, input_z, pitch;
	u_int32_t color_dot = 0xFF0FFF1F;

	unsigned char cont = 1;
	unsigned int old_x, old_y, read_samples=0;

	ui_t * window;
	input_t * input_data;

	window = ui_init(myCfg->scope.width, myCfg->scope.height);
	window->weight = myCfg->scope.weight;
	ui_set_bgcolor(window, myCfg->scope.phosphore<<24);

	myCfg->scope.x = ((float) myCfg->scope.width) * 0.5f;
	myCfg->scope.y = ((float) myCfg->scope.height) * 0.5f;

	old_x = (unsigned int) myCfg->scope.x;
	old_y = (unsigned int) myCfg->scope.y;

	myCfg->scope.refresh_count = ((float) myCfg->rate) / myCfg->scope.fps;


	myCfg->paused = 0;

	// Calculate Coefficient (X=Y, CoefH is not used here)
	myCfg->scope.CoefV = (((float) myCfg->scope.height) * 0.5f) / myCfg->scope.SensV;


	if (myCfg->channels != (2 + (myCfg->scope.z))) {
		syslog(LOG_ERR, "osc: Must have 2 (or 3 with -z) channels in XY mode");
		ui_free(window);
		return;
	}
	
	input_data = input_init(myCfg->scope.refresh_count * 2, myCfg);
	
	// No input, cannot continue.
	if (input_data == NULL) {
		syslog(LOG_ERR, "osc : Input module can't be initialized, aborting");
		ui_free(window);
		return;
	}

	while (cont != 0) {
		input_x = buffer_read_sample(input_data);
		input_y = buffer_read_sample(input_data);

		if (myCfg->scope.z>0) {
			input_z = buffer_read_sample(input_data);
		} else {
			input_z = 1.f;
		}

		if (myCfg->paused == 0) {
			read_samples++;

			myCfg->scope.x = ((float) myCfg->scope.width)*0.5f + (input_x*myCfg->scope.CoefV);
			myCfg->scope.y = ((float) myCfg->scope.height)*0.5f - (input_y*myCfg->scope.CoefV);

			pitch = 255.f * (myCfg->scope.pitch / pow(sqrt(pow(((float) old_x) - myCfg->scope.x,2) + pow(((float) old_y) - myCfg->scope.y,2)),2));
			pitch = ((input_z<0)?0.f:(input_z * myCfg->scope.pitch)) * pitch;
			pitch = (pitch>255.f)? 255.f : pitch;


			// Draw osc line(s)
			ui_drawline(old_x, old_y, (unsigned int) myCfg->scope.x, (unsigned int) myCfg->scope.y, (color_dot&0xFFFFFF) + (((unsigned int) pitch)<<24), window);

			old_x = (unsigned int) myCfg->scope.x;
			old_y = (unsigned int) myCfg->scope.y;

			if (read_samples==myCfg->scope.refresh_count) {
				ui_refresh(myCfg, window);
				read_samples = 0;
			}
		}

		if (evt_osc(myCfg, window, input_data)<0) {
			cont = 0;
		}
	}

	input_clean(input_data);
	ui_free(window);
}
