/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include <visound/core/modules.h>
#include <visound/modules/events.h>
#include <visound/modules/input/global.h>
#include <visound/ui/global.h>

/**
 * Oscilloscope XY module
 * @author Adrien SOHIER
 */

/**
 * Run oscilloscope with 2-way XY mode
 * @param myCfg The config storage structure
 */
void osc_xy3D_run(config_t * myCfg)
{
	syslog(LOG_INFO, "Oscilloscope XY-3D started.");
	float x1, y1, x2, y2, z, locZ, pitch;
	u_int32_t color_dot = 0xFF0FFF1F, readSamples=0;
	int i, toRead;

	unsigned char cont = 1;

	ui_t * window;
	input_t * input_data;

	window = ui_init(myCfg->scope.width, myCfg->scope.height);
	window->weight = myCfg->scope.weight;
	ui_set_bgcolor(window, 0xFF000000);

	// Sets config data
	myCfg->paused = 0;
	myCfg->scope.CoefV = 150;
	myCfg->scope.refresh_count = (unsigned int) (((float) myCfg->rate) / myCfg->scope.fps);
	myCfg->xy3D.curIdx = 0;
	myCfg->xy3D.lastIdx = 1;

	// Calculate coefficients and allocate buffers
	myCfg->xy3D.bufSize = (int) (((float) myCfg->rate) * myCfg->scope.SensH);
	myCfg->xy3D.Cz = XY3D_Z_MAX / ((float) myCfg->xy3D.bufSize);
	myCfg->xy3D.buffer.x = malloc(sizeof(float) * myCfg->xy3D.bufSize);
	myCfg->xy3D.buffer.y = malloc(sizeof(float) * myCfg->xy3D.bufSize);
	for(i=0 ; i<myCfg->xy3D.bufSize ; i++) {
		myCfg->xy3D.buffer.x[i] = 0.f;
		myCfg->xy3D.buffer.y[i] = 0.f;
	}

	// XY Mode really means there's an X and Y channel. So if you are dumb enougth not to
	// provide me 2 channels, too bad for you ! I can't work without it.
	if (myCfg->channels != 2 ) {
		syslog(LOG_ERR, "xy3D Must have 2 channels in XY mode");
		ui_free(window);
		free(myCfg->xy3D.buffer.x);
		free(myCfg->xy3D.buffer.y);
		return;
	}
	
	input_data = input_init(myCfg->scope.refresh_count * 2, myCfg);
	
	// No input, cannot continue.
	if (input_data == NULL) {
		syslog(LOG_ERR, "xy3D : Input module can't be initialized, aborting");
		ui_free(window);
		return;
	}

	while (cont != 0) {
		// Read input data if we have nothing to keep to display
		toRead = (myCfg->scope.fillscreen==0)? myCfg->scope.refresh_count : 1;

		if (readSamples<(myCfg->xy3D.bufSize-1) || myCfg->scope.fillscreen==0) {
			for(i=0 ; i<toRead ; i++) {
				myCfg->xy3D.buffer.x[myCfg->xy3D.curIdx] = buffer_read_sample(input_data);
				myCfg->xy3D.buffer.y[myCfg->xy3D.curIdx] = buffer_read_sample(input_data);
			
				if (i+1 < toRead) {
					myCfg->xy3D.curIdx = myCfg->xy3D.lastIdx;
					myCfg->xy3D.lastIdx++;
					if (myCfg->xy3D.lastIdx>=myCfg->xy3D.bufSize)
						myCfg->xy3D.lastIdx = 0;
				}
			}
		}

		if (myCfg->paused == 0) {
			readSamples+=toRead;

			// Buffer filled or synchro mode and refresh rate hit, display Master !
			if ((readSamples >= myCfg->xy3D.bufSize && myCfg->scope.fillscreen>0) ||
			    (readSamples >= myCfg->scope.refresh_count && myCfg->scope.fillscreen==0)) {
				x1 = 202;
				y1 = 205;
				z = 1;

				// Draw axis to help visualizing the signal
				ui_drawline((int) (x1-0.8660254*150), (int) (y1+75), (int) (x1+0.8660254*150), (int) (y1-75), color_dot, window);
				ui_drawline((int) x1, (int) (y1-150), (int) x1, (int) (y1+150), color_dot, window);
				ui_drawline((int) x1, (int) y1, (int) (x1+((float) myCfg->xy3D.bufSize)*myCfg->xy3D.Cz*0.8660254), (int) (y1+((float) myCfg->xy3D.bufSize)*myCfg->xy3D.Cz*0.5), color_dot, window);

				// Draw the ring buffer 
				for(i=myCfg->xy3D.lastIdx ; i!=myCfg->xy3D.curIdx ; i++) {
					// XYZ Projection computations (what do you mean, « you should use OpenGL » ? I don't care ! :D)
					locZ = 0.5 + (z / ((float) myCfg->xy3D.bufSize)*0.5);
					x2 = 202 + z*myCfg->xy3D.Cz*0.8660254*locZ + myCfg->xy3D.buffer.x[i]*0.8660254*myCfg->scope.CoefV*locZ;
					y2 = 205 + z*myCfg->xy3D.Cz*0.5*locZ - myCfg->xy3D.buffer.x[i]*0.5*myCfg->scope.CoefV*locZ - myCfg->xy3D.buffer.y[i]*myCfg->scope.CoefV*locZ;
					z+=1.0;

					// Computes the intensity of the spot by estimating it's speed (remember : we simulate an electron beam)
					pitch = 255.f * (myCfg->scope.pitch / pow(sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2)),2));
					pitch = (z == 2.0)? 255.f : ((pitch>255.f)? 255.f : pitch);
					
					// Finally : draw the line ! Phew !
					ui_drawline((int) x1, (int) y1, (int) x2, (int) y2, (color_dot&0xFFFFFF) + (((unsigned int) pitch)<<24), window);
					x1 = x2;
					y1 = y2;
					if (i==myCfg->xy3D.bufSize-1)
						i = -1;
				}

				// Send screen buffer to the graphic system stack
				ui_refresh(myCfg, window);
				readSamples = 0;
			}
		}

		// If we have nothing special to keep (eg end of buffer in synchro mode), gets the next index to point at
		if (readSamples<(myCfg->xy3D.bufSize-1) || myCfg->scope.fillscreen==0) {
			myCfg->xy3D.curIdx = myCfg->xy3D.lastIdx;
			myCfg->xy3D.lastIdx++;
			if (myCfg->xy3D.lastIdx>=myCfg->xy3D.bufSize)
				myCfg->xy3D.lastIdx = 0;
	
			if (evt_osc(myCfg, window, input_data)<0) {
				cont = 0;
			}
		}
	}

	// Bye bye, data ! We love you !
	input_clean(input_data);
	ui_free(window);
	free(myCfg->xy3D.buffer.x);
	free(myCfg->xy3D.buffer.y);
}
