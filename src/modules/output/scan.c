/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#include <stdio.h>

#include <visound/core/modules.h>
#include <visound/modules/events.h>
#include <visound/modules/input/global.h>
#include <visound/ui/global.h>

/**
 * Scan module
 * @author Adrien Sohier
 */


/**
 * Run scanner module
 * @param myCfg The configuration storage structure
 */
void scan_run(config_t * myCfg)
{
	syslog(LOG_INFO, "Scan mode started.");
	unsigned char cont = 1;
	float * input = (float *) calloc(myCfg->scan.subchannels, sizeof(float));
	u_int32_t color_dot;
	unsigned int i;

	// Calculate real size
	myCfg->scan.real.height = myCfg->scan.FreqH / myCfg->scan.FreqV;
	myCfg->scan.real.width = (float) (myCfg->rate*myCfg->channels) / (myCfg->scan.FreqH * myCfg->scan.subchannels);
	syslog(LOG_INFO, "scan : real size is %f x %f\n", myCfg->scan.real.width, myCfg->scan.real.height);

	ui_t * window;
	input_t * input_data;

	window = ui_init(myCfg->scan.coef * (1+((unsigned int) myCfg->scan.real.width)), myCfg->scan.coef * (1+((unsigned int) myCfg->scan.real.height)));
	ui_set_coef(window, myCfg->scan.coef);

	// Init data
	myCfg->scan.deltaH = 0.f;
	myCfg->scan.deltaV = 0.f;

	myCfg->scan.x = 0;
	myCfg->scan.y = 0;

	myCfg->scan.used.width = ((unsigned int) myCfg->scan.real.width);
	myCfg->scan.used.height = ((unsigned int) myCfg->scan.real.height);
	input_data = input_init(myCfg->scan.used.width*myCfg->scan.used.height, myCfg);

	myCfg->paused = 0;

	while (cont == 1) {
		color_dot = 0x000000;
		for (i=0 ; i<myCfg->scan.subchannels ; i++) {
			input[i] = 127.f + ( 127.f * myCfg->scan.ampli * buffer_read_sample(input_data) );
			input[i] = (input[i]<0)?0.f:((input[i]>255)?255.f:input[i]);

			color_dot += ( ((u_int32_t)(myCfg->scan.subcolors.red[i] * input[i])<<16) +
			               ((u_int32_t)(myCfg->scan.subcolors.green[i] * input[i])<<8) +
			               ((u_int32_t)(myCfg->scan.subcolors.blue[i] * input[i])) );
		}

		if (myCfg->paused == 0) {

			ui_drawpoint(myCfg->scan.coef*myCfg->scan.x, myCfg->scan.coef*myCfg->scan.y, color_dot, window);

			myCfg->scan.x++;
			if (myCfg->scan.x == myCfg->scan.used.width) {
				myCfg->scan.x = 0;
				myCfg->scan.y++;
				myCfg->scan.deltaH += myCfg->scan.real.width - ((float) myCfg->scan.used.width);
				myCfg->scan.used.width = ((unsigned int) myCfg->scan.real.width) + ((unsigned int) myCfg->scan.deltaH);
				myCfg->scan.deltaH -= ((unsigned int) myCfg->scan.deltaH);
			}
			if (myCfg->scan.y == myCfg->scan.used.height) {
				ui_refresh(myCfg, window);
				myCfg->scan.y = 0;
				myCfg->scan.deltaV += myCfg->scan.real.height - ((float) myCfg->scan.used.height);
				myCfg->scan.used.height = ((unsigned int) myCfg->scan.real.height) + ((unsigned int) myCfg->scan.deltaV);
				myCfg->scan.deltaV -= ((unsigned int) myCfg->scan.deltaV);
			}
		}

		if (evt_scan(myCfg, window, input_data)<0) {
			cont = 0;
		}
	}

	free(input);
	input_clean(input_data);
	ui_free(window);
}

/**
 * @brief scan_allocateChannels Allocate channels accordingly to the subschannels count
 * @param myCfg The config tu use
 * @return	0: everything went well.
 *         -1: Pointer problem
 */
int scan_allocateChannels(config_t * myCfg)
{
	if (myCfg == NULL)
		return -1;

	if (myCfg->scan.subcolors.red != NULL)
		free(myCfg->scan.subcolors.red);
	myCfg->scan.subcolors.red = (float *) calloc(myCfg->scan.subchannels, sizeof(float));
	if (myCfg->scan.subcolors.red == NULL)
		return -1;

	if (myCfg->scan.subcolors.green != NULL)
		free(myCfg->scan.subcolors.green);
	myCfg->scan.subcolors.green = (float *) calloc(myCfg->scan.subchannels, sizeof(float));
	if (myCfg->scan.subcolors.green == NULL) {
		free(myCfg->scan.subcolors.red);
		return -1;
	}

	if (myCfg->scan.subcolors.blue != NULL)
		free(myCfg->scan.subcolors.blue);
	myCfg->scan.subcolors.blue = (float *) calloc(myCfg->scan.subchannels, sizeof(float));
	if (myCfg->scan.subcolors.blue == NULL) {
		free(myCfg->scan.subcolors.red);
		free(myCfg->scan.subcolors.green);
		return -1;
	}

	return 0;
}
