/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>

#ifdef SDL20

#include <visound/ui/SDL20.h>

/**
 * SDL 2.0 Interface
 * @author Adrien Sohier
 */

/**
 * @brief ui_init Creates the SDL2 window & renderer
 * @param width The window's width
 * @param height The window's height
 * @return The initialized data structure
 */
ui_t * ui_init(unsigned int width, unsigned int height)
{
	ui_t * data = malloc(sizeof(ui_t));

	SDL_Init(SDL_INIT_VIDEO);

	data->window = SDL_CreateWindow("Modular Sound Visualizer", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN);
	if(!data->window)
	{
		syslog(LOG_ERR, "No X11 Display available.");
		exit(1);
	}
	data->render = SDL_CreateRenderer(data->window, -1, SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawBlendMode(data->render, SDL_BLENDMODE_BLEND);

	data->rect.w = data->rect.h = 1;
	data->bgcolor = 0x000000;
	data->width = width;
	data->height = height;
	data->weight = 2;

	data->surface = SDL_CreateRGBSurface(0, width, height, 32, 0, 0, 0, 0);

	return data;
}

/**
 * @brief ui_free Frees everything
 * @param data The data structure
 */
void ui_free(ui_t * data)
{
	SDL_FreeSurface(data->surface);
	SDL_DestroyRenderer(data->render);
	SDL_DestroyWindow(data->window);

	SDL_Quit();
}

/**
 * @brief ui_drawpoint Draw a point on the screen
 * @param x The horizontal position of the point
 * @param y The vertical position of the point
 * @param color The point's color
 * @param data The data structure
 */
void ui_drawpoint(unsigned int x, unsigned int y, u_int32_t color, ui_t * data)
{
	data->rect.x = x;
	data->rect.y = y;

	SDL_SetRenderDrawBlendMode(data->render, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(data->render, (color&0xFF0000)>>16, (color&0xFF00)>>8, (color&0xFF), 0xFF);
	SDL_RenderFillRect(data->render, &(data->rect));
}

/**
 * @brief ui_drawline Draw a line on the screen
 * @param x1 The horizontal position of the first point
 * @param y1 The vertical position of the first point
 * @param x2 The horizontal position of the second point
 * @param y2 The vertical position of the second point
 * @param color The line's color
 * @param data The data structure
 */
void ui_drawline(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, u_int32_t color, ui_t * data)
{
	int i;
	SDL_SetRenderDrawBlendMode(data->render, SDL_BLENDMODE_ADD);
	SDL_SetRenderDrawColor(data->render, (color&0xFF0000)>>16, (color&0xFF00)>>8, color&0xFF, (color&0xFF000000)>>24);
	if (data->weight>0) {
		for(i=-data->weight; i<=data->weight; i++) {
			SDL_SetRenderDrawColor(data->render, (color&0xFF0000)>>16, (color&0xFF00)>>8, color&0xFF, (((color&0xFF000000)>>24) * (data->weight-abs(i))) / data->weight );
			SDL_RenderDrawLine(data->render, x1, y1+i,  x2, y2+i);
			SDL_RenderDrawLine(data->render, x1+i, y1,  x2+i, y2);
		}
	} else {
		SDL_SetRenderDrawColor(data->render, (color&0xFF0000)>>16, (color&0xFF00)>>8, color&0xFF, ((color&0xFF000000)>>24));
		SDL_RenderDrawLine(data->render, x1, y1,  x2, y2);
	}
}

/**
 * @brief ui_refresh Refreshes the screen
 * @param data The data structure
 */
void ui_refresh(config_t * myCfg, ui_t * data)
{
	char str[255];

	SDL_RenderPresent(data->render);
	if (myCfg->saving == 1) {
		sprintf(str, "%s%08d.bmp", myCfg->outputDirName, myCfg->current_frame);
		myCfg->current_frame++;
		ui_saveToFile(data, str);
	}

	SDL_SetRenderDrawBlendMode(data->render, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(data->render, 0, 0, 0, (data->bgcolor&0xFF000000)>>24);
	SDL_RenderFillRect(data->render, &data->surface->clip_rect);
}

/**
 * Get an event from SDL
 * @param data The data structure to use
 * @return 0 : no event
 *         1 : some event caught
 */
int ui_getEvent(ui_t * data)
{
	int ret;

	ret = SDL_PollEvent(&(data->event));

	return ret;
}

/**
 * Set the BG Color
 * @param data The data structure to use
 * @param color the color's value
 */
void ui_set_bgcolor(ui_t * data, u_int32_t color)
{
	data->bgcolor = color;
}

/**
 * Change zoom
 * @param data The data structure to use
 * @param coef The new coef
 */
void ui_set_coef(ui_t* data, unsigned int coef)
{
	data->rect.h = data->rect.w = coef;
}

/**
 * Print on the standard output the backend version
 */
void ui_print_version()
{
	printf("— SDL20 (SDL 2.0.1 UI)\n");
}

/**
 * @brief Save a picture from the current screen
 * @param data The data structure to use
 * @param filename The filename to save to
 * @return 0: ok, <>0: error
 */
int ui_saveToFile(ui_t * data, char * filename)
{
	SDL_UnlockSurface(data->surface);
	SDL_RenderReadPixels(data->render, &data->surface->clip_rect, data->surface->format->format, data->surface->pixels, data->surface->pitch);
	SDL_LockSurface(data->surface);
	return SDL_SaveBMP(data->surface, filename);
}

#endif
