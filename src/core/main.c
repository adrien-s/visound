/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>

#include <visound/core/config.h>
#include <visound/core/modules.h>
#include <visound/core/parse.h>

/**
 * @brief Init the config
 * @param myCfg the config to ini
 */
void init_cfg(config_t * myCfg)
{

	// Global
	myCfg->mode = SCANNER;
	myCfg->rate = 44100;
	myCfg->channels = 1;
	myCfg->saving = 0;
	myCfg->outputDirName = NULL;
	myCfg->current_frame = 0;

	// Scanner
	myCfg->scan.coef = 1.f;
	myCfg->scan.ampli = 1.f;
	myCfg->scan.FreqV = 10.7666015625;
	myCfg->scan.FreqH = 689.0625;
	myCfg->scan.subcolors.red = myCfg->scan.subcolors.green = myCfg->scan.subcolors.blue = NULL;
	myCfg->scan.subchannels = 1;
	scan_allocateChannels(myCfg);
	myCfg->scan.subcolors.red[0] = 1.f;
	myCfg->scan.subcolors.green[0] = 1.f;
	myCfg->scan.subcolors.blue[0] = 1.f;

	// Osc
	myCfg->scope.width = 1024;
	myCfg->scope.height = 768;
	myCfg->scope.phosphore = 0x7F;
	myCfg->scope.fps = 30.f;
	myCfg->scope.fillscreen = 0;
	myCfg->scope.SensV = 1.f;
	myCfg->scope.SensH = 1e-3;
	myCfg->scope.SensH_pow = -3;
	myCfg->scope.SensH_abs = 1;
	myCfg->scope.channels_count = 1;
	myCfg->scope.trigger.channel = -1;
	myCfg->scope.pitch = 1.0;
	myCfg->scope.z = 0;
	myCfg->scope.weight = 2;

	// Input
	myCfg->input.type = type_alsa;
	myCfg->input.alsa.device = "default";

}

/**
 * Main method
 * @author Adrien SOHIER
 */

int main(int argc, char ** argv) {

	config_t myCfg;
	int ret;

	openlog("ViSound", LOG_PID|LOG_ODELAY, LOG_USER);

	init_cfg(&myCfg);
	ret = parse(argc, argv, &myCfg);

	closelog();
	return ret;
}
