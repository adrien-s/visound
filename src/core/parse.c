/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>

#include <visound/core/config.h>
#include <visound/core/parse.h>
#include <visound/core/modules.h>
#include <visound/ui/global.h>

/**
 * Parse command line arguments
 * @author Adrien SOHIER
 */


/**
 * Parse command line arguments and fills the configuration storage structure.
 * @param argc The input arguments count (program path counted)
 * @param argv The arguments array
 * @return  0 : Stop normally (help displayed, etc… )
 *         -1 : Abnormal ternimation (bad argument, etc… )
 */
int parse(int argc, char ** argv, config_t * myCfg)
{
	int return_code;
	if (myCfg == NULL)
		return -1;

	// No args: display some help
	if (argc == 1) {
		parse_display_help(argv[0], GLOBAL);
	}

	int nb = 1;
	while (nb<argc) {
		// help
		if (strcmp(argv[nb], "-h") == 0 || strcmp(argv[nb], "--help") == 0) {
			parse_display_help(argv[0], GLOBAL);
			return 0;
		}
		//version + options
		if (strcmp(argv[nb], "-v") == 0 || strcmp(argv[nb], "--version") == 0) {
			printf("ViSound version %s\nMaintained by : %s\nContributor(s) : \n%s\nThis is FREE SOFTWARE.\n\n", VERSION, MAINTAINER, CONTRIBUTORS);
            printf("Compilation defines :\n");
            ui_print_version();
            printf("\nThis software is licenced under the GNU GPL version 3 or above\n\
(See COPYING for details)\n\
Support : %s\n", SUPPORTMAIL);
			       nb++;
			        return 0;
		}
		// verbose
		if (strcmp(argv[nb], "-V")==0 || strcmp(argv[nb], "--verbose")==0) {
			closelog();
			openlog("ViSound", LOG_PID|LOG_ODELAY|LOG_PERROR, LOG_USER);
			nb++;
		}
		//rate
		if ((strcmp(argv[nb], "-r") == 0 || strcmp(argv[nb], "--rate") == 0) && nb+1<argc) {
			myCfg->rate = atoi(argv[nb+1]);
			nb+=2;
			continue;
		}
		//channels
		if ((strcmp(argv[nb], "-c") == 0 || strcmp(argv[nb], "--channels") == 0) && nb+1<argc) {
			myCfg->channels = atoi(argv[nb+1]);
			nb+=2;
			continue;
		}

		//input
		if ((strcmp(argv[nb], "-i") == 0 || strcmp(argv[nb], "--input") == 0) && nb+1<argc) {
			if (parse_input(&nb, argc, argv, myCfg)==0) {
				nb++;
				continue;
			}
		}

		// save
		if ((strcmp(argv[nb], "-s") == 0 || strcmp(argv[nb], "--save") == 0) && nb+1<argc) {
			myCfg->outputDirName = argv[nb+1];
			nb+=2;
			continue;
		}
		// Res for osc / oscxy / xy3d modes
		if ((strcmp(argv[nb], "-w") == 0 || strcmp(argv[nb], "--width") == 0) && nb+1<argc) {
			myCfg->scope.width = atoi(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "-H") == 0 || strcmp(argv[nb], "--height") == 0) && nb+1<argc) {
			myCfg->scope.height = atoi(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "-W") == 0 || strcmp(argv[nb], "--weight") == 0) && nb+1<argc) {
			myCfg->scope.weight = atoi(argv[nb+1]);
			nb += 2;
			continue;
		}


		//mode
		if (strcmp(argv[nb], "-m") == 0 || strcmp(argv[nb], "--mode") == 0) {
			if (nb+1<argc) {
				if (strcmp(argv[nb+1], "scan") == 0) {
					myCfg->mode = SCANNER;
					nb+=2;
					break;
				}
				if (strcmp(argv[nb+1], "osc") == 0) {
					myCfg->mode = SCOPE;
					nb+=2;
					break;
				}
				if (strcmp(argv[nb+1], "xy") == 0) {
					myCfg->mode = SCOPEXY;
					nb+=2;
					break;
				}
				if (strcmp(argv[nb+1], "3d") == 0) {
					myCfg->mode = SCOPEXY3D;
					nb+=2;
					break;
				}
			}
			syslog(LOG_ERR, "Required mode:\n- scan (scan mode)\n- osc (oscillo)\n- xy (oscillo XY mode)\n- 3d (oscillo XY 3D mode)\n");
			return -1;
		}
		syslog(LOG_ERR, "unknown switch: %s\ntry %s -h or %s --help.\n", argv[nb], argv[0], argv[0]);
		return -1;
	}

	//mode switchs
	switch (myCfg->mode) {
	case SCANNER:
		return_code = parse_scan(nb, argc, argv, myCfg);
		if (return_code == 0) {
			syslog(LOG_INFO, "Parsing OK, starting scan module");
			scan_run(myCfg);
		} else if (return_code < -1) {
			syslog(LOG_ERR, "parse_scan caught an error : %d\n", return_code);

		}
		break;
	case SCOPE:
		return_code = parse_scope(nb, argc, argv, myCfg);
		if (return_code == 0) {
			syslog(LOG_INFO, "Parsing OK, starting osc module");
			osc_run(myCfg);
		} else if (return_code < -1) {
			syslog(LOG_ERR, "parse_scope caught an error : %d\n", return_code);
		}
		break;
	case SCOPEXY:
		return_code = parse_scope_xy(nb, argc, argv, myCfg);
		if (return_code == 0) {
			syslog(LOG_INFO, "Parsing OK, starting oscxy module");
			osc_xy_run(myCfg);
		} else if (return_code < -1) {
			syslog(LOG_ERR, "parse_scope_xy caught an error : %d\n", return_code);
		}
		break;
	case SCOPEXY3D:
		return_code = parse_scope_xy3d(nb, argc, argv, myCfg);
		if (return_code == 0) {
			syslog(LOG_INFO, "Parsing OK, starting xy3D module");
			osc_xy3D_run(myCfg);
		} else if (return_code < -1) {
			syslog(LOG_ERR, "parse_scope_xy caught an error : %d\n", return_code);
		}
		break;
	default:
		return_code = -2;
		break;
	}
	return return_code + 1;
}

/**
 * Display program command line help.
 * @param prog_name The name of the program (ie argv[0])
 * @param p The help page
 */
void parse_display_help(char * prog_name, help_page p)
{
	switch (p) {
	case GLOBAL:
		fprintf(stdout, "ViSound %s ─ Maintained by %s\n\n\
%s [-h] [-v] [-V] [-r <rate>] [-c <channels>] [-i <input> <input options>] [-w <width>] [-H <height>] [-W <weight>][-m <mode>]\n\
%s [--help] [--version] [--verbose] [--rate <rate>] [--channels <channels>] [--input <input> <input options>] [--width <width>] [--height <height>] [--weight <weight>] [--mode <mode>]\n\
-h, --help\n\
	Displays this help screen.\n\
-v, --version\n\
	Displays version info.\n\
-V, --verbose\n\
	Displays log in the terminal\n\
-w, --width\n\
	Window width for osc modes\n\
-H, --height\n\
	Window height for osc modes\n\
-m, --mode\n\
	Set display mode.\n\
	Can be: scan (scan mode), osc (scope), xy (scope, XY 2-way mode)\n\
	WARNING: must be last GLOBAL option, since it switches to mode's options from here !\n\
	You can obtain help for each mode by typing --mode <yourmode> --help.\n\
-r, --rate\n\
	Input sound rate (eg 44100, 48000, 192000 and so on)\n\
	It obviously should match real input rate, as it relies on this rate to make internal calculations such as refresh rate and screen size (for scanning).\n\
-c, --channels\n\
    Number of channels in the input.\n\
-i, --input\n\
    Selects the input\n\
    - / stdin :\n\t\tstandard input (no options)\n\
    file <filename> :\n\t\tInputs from a file\n\
    alsa <device> : \n\t\tInputs from alsa (device is like hw:x[,y])\n\
	jackd :\n\t\tInputs from Jackd, will create input port(s) that you'll be able to connect to what you want to analyze.\n\
-s, --save <dir/prefix>\n\
	Save frames to dir/prefix××××××××.bmp when s key pressed\n\
-W, --weight <weight>\n\
	Set line width for oscilloscope modes.\n", VERSION, MAINTAINER, prog_name, prog_name);
		break;
	case SCAN:
		fprintf(stdout, "ViSound %s − Maintained by %s\n\n\
 ==> Scan\n\
short: [-H <freq>] [-v <freq>] [-s <val>] [-a <value>] [-ch <value> [-co <num> <r> <g> <b>]…]\n\
long : [--horizontal <freq>] [--vertical <freq>] [--size <val>] [--ampli <value>] [--channels <value> [--color <num> <r> <g> <b>]…]\n\n\
-H, --horizontal:\n\
	(float) Horizontal frequency, ie number of scan lines in a second.\n\
-v, --vertical:\n\
	(float) Vertical frequency, ie framerate.\n\
-s, --size :\n\
	Size of a pixel.\n\
-a, --ampli : \n\
	(float) amplifies sound.\n\
-ch, --channels : \n\
        Number of channels to display\n\
-co, --color : \n\
        Arguments : channel to set, red value, green value, blue value (0≤x≤1)\n\
        Sets the color coefficient for a specified channel\n", VERSION, MAINTAINER);
		break;
	case OSC:
		fprintf(stdout, "ViSound %s − Maintained by %s\n\n\
 ==> Oscilloscope\n\
short: [-v <sensv>] [-H <sensh>] [-p <val>] [-r <val>] [-f] [-tl <float>] [-tc <integer>] [-tp <float>] [-pi <coef>]\n\
long : [--vertical <sensv>] [--horizontal <sensh>] [--phosphore <val>] [--refresh <val>] [--fill] [--trigger-level <float>] [--trigger-channel <integer>] [--trigger-precision <float>] [--pitch <coef>]\n\n\
-v, --vertical:\n\
	(float number) Vertical sensivity in units / division (8 divisions = all screen's height)\n\
-H, --horizontal:\n\
	(float number) Horizontal sensivity in seconds / division (10 divisions = all screen's width)\n\
-p, --phosphore :\n\
	Screen's speed for fading pixels. 0=no fade, 255=maximum fade\n\
-r, --rate :\n\
	Number of pictures/second\n\
-f, --fill :\n\
	Fills the screen before updating it\n\
-tl, --trigger-level :\n\
	Trigger sync level (-1←→1)\n\
-tc, --trigger-channel :\n\
	Trigger sync channel (0→number of channels - 1)\n\
-tp, --trigger-precision :\n\
	Trigger dot precision\n\
-pi, --pitch:\n\
	Pitch scale\n", VERSION, MAINTAINER);
		break;
	case XY:
		fprintf(stdout, "ViSound %s − Maintained by %s\n\n\
 ==> Oscilloscope XY-Mode\n\
short: [-s <coef>] [-r <val>] [-p <val>] [-pi <coef>] [-z]\n\
long : [--sensivity <coef>] [--refresh <val>] [--phosphore <val>] [--pitch <coef>] [--z-axis]\n\n\
-s, --sensivity:\n\
	(float number) Sensivity, Amplification coefficient (1 => top edge of the screen in full scale)\n\
-p, --phosphore :\n\
	Screen's speed for fading pixels. 0=no fade, 255=maximum fade\n\
-r, --rate :\n\
	Number of pictures/second\n\
-pi, --pitch:\n\
	Pitch scale\n\
-z, --z-axis:\n\
	Adds a 3rd input channel to modulate the spot intensity\n", VERSION, MAINTAINER);
		break;
	case XY3D:
		fprintf(stdout, "ViSound %s − Maintained by %s\n\n\
 ==> Oscilloscope XY 3D Mode\n\
short: [-r <val>] [-pi <val>] [-z <float>] [-s] [-p <val>]\n\
long : [--refresh <val>] [--pitch <val>] [--z-step <float>] [--synchro] [--phosphor <val>]\n\n\
-r, --rate :\n\
	Number of pictures/second\n\
-pi, --pitch:\n\
	Pitch scale\n\
-p, --phosphore :\n\
	Screen's speed for fading pixels. 0=no fade, 255=maximum fade\n\
-s, --synchro:\n\
	Synchronizes the trace to the buffer\n\
-z, --z-step :\n\
	Step of Z axis (1.0 by default)\n", VERSION, MAINTAINER);
		break;
	default:
		break;
	}
}

/**
 * Parse arguments for scan
 * @param nb The current argument who was being parsed
 * @param argc The number of arguments
 * @param argv The arguments
 * @param myCfg The config_t struct
 * @return 0: OK
 *        -1: must exit.
 *        -2: something bad happened.
 */
int parse_scan(int nb, int argc, char ** argv, config_t * myCfg)
{
	while (nb<argc) {
		if (strcmp(argv[nb], "-h") == 0 || strcmp(argv[nb], "--help") == 0) {
			parse_display_help(argv[0], SCAN);
			return -1;
		}
		if ((strcmp(argv[nb], "-a")==0 || strcmp(argv[nb], "--ampli")==0) && nb+1<argc) {
			myCfg->scan.ampli = atof(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "-ch") == 0 || strcmp(argv[nb], "--channels") == 0) && nb+1<argc) {
			myCfg->scan.subchannels = atoi(argv[nb+1]);
			if (scan_allocateChannels(myCfg)!=0)
			{
				syslog(LOG_ERR, "Could not allocate channels data space for scan mode.\n");
				return -2;
			} else {
				nb+=2;
				continue;
			}
		}
		if ((strcmp(argv[nb], "-co") == 0 || strcmp(argv[nb], "--color") == 0) && nb+4<argc) {
			int ch = atoi(argv[nb+1]);
			if (ch<myCfg->scan.subchannels) {
				myCfg->scan.subcolors.red[ch] = atof(argv[nb+2]);
				myCfg->scan.subcolors.green[ch] = atof(argv[nb+3]);
				myCfg->scan.subcolors.blue[ch] = atof(argv[nb+4]);
			}
			nb+=5;
			continue;
		}
		if ((strcmp(argv[nb], "-H") == 0 || strcmp(argv[nb], "--horizontal") == 0) && nb+1<argc) {
			myCfg->scan.FreqH = atof(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "-v") == 0 || strcmp(argv[nb], "--vertical") == 0) && nb+1<argc) {
			myCfg->scan.FreqV = atof(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "-s")==0 || strcmp(argv[nb], "--size")==0) && nb+1<argc) {
			myCfg->scan.coef = atoi(argv[nb+1]);
			nb+=2;
			continue;
		}
		syslog(LOG_ERR, "Switch %s unknown ! try -h or --help.\n", argv[nb]);
		return -2;
	}
	return 0;
}

/**
 * Parse arguments for oscilloscope
 * @param nb The current argument who was being parsed
 * @param argc The number of arguments
 * @param argv The arguments
 * @param myCfg The config_t struct
 * @return 0: everything is ok
 *        -1: must exit
 *        -2: something bad happened
 */
int parse_scope(int nb, int argc, char ** argv, config_t * myCfg)
{
	while (nb<argc) {
		if (strcmp(argv[nb], "-h") == 0 || strcmp(argv[nb], "--help") == 0) {
			parse_display_help(argv[0], OSC);
			return -1;
		}
		if ((strcmp(argv[nb], "--vertical") == 0 || strcmp(argv[nb], "-v") == 0) && nb+1<argc) {
			myCfg->scope.SensV = atof(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "--horizontal") == 0 || strcmp(argv[nb], "-H") == 0) && nb+1<argc) {
			myCfg->scope.SensH = atof(argv[nb+1]);
			myCfg->scope.SensH_pow = 0;
			float f = myCfg->scope.SensH;
			while (f<1.f) {
				f*=10.f;
				myCfg->scope.SensH_pow--;
			}
			while (f>=10.f) {
				f*=0.1f;
				myCfg->scope.SensH_pow++;
			}
			myCfg->scope.SensH_abs = f;
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "-p")==0 || strcmp(argv[nb], "--phosphore")==0) && nb+1<argc) {
			myCfg->scope.phosphore = atoi(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "-r")==0 || strcmp(argv[nb], "--rate")==0) && nb+1<argc) {
			myCfg->scope.fps = atof(argv[nb+1]);
			nb+=2;
			continue;
		}
		if (strcmp(argv[nb], "-f")==0 || strcmp(argv[nb], "--fill")==0) {
			myCfg->scope.fillscreen = 1;
			nb++;
			continue;
		}
		if ((strcmp(argv[nb], "-tc")==0 || strcmp(argv[nb], "--trigger-channel")==0) && nb+1<argc) {
			myCfg->scope.trigger.channel = atoi(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "-tl")==0 || strcmp(argv[nb], "--trigger-level")==0) && nb+1<argc) {
			myCfg->scope.trigger.level = atof(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "-tp")==0 || strcmp(argv[nb], "--trigger-precision")==0) && nb+1<argc) {
			myCfg->scope.trigger.precision = atof(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "--pitch") == 0 || strcmp(argv[nb], "-pi") == 0) && nb+1<argc) {
			myCfg->scope.pitch = atof(argv[nb+1]);
			nb+=2;
			continue;
		}
		syslog(LOG_ERR, "Switch %s unknown ! try -h or --help.\n", argv[nb]);
		return -2;
	}
	return 0;
}

/**
 * Parse arguments for oscilloscope XY
 * @param nb The current argument who was being parsed
 * @param argc The number of arguments
 * @param argv The arguments
 * @param myCfg The config_t struct
 * @return 0: everything is ok
 *        -1: must exit
 *        -2: something bad happened
 */
int parse_scope_xy(int nb, int argc, char ** argv, config_t * myCfg)
{
	while (nb<argc) {
		if (strcmp(argv[nb], "-h") == 0 || strcmp(argv[nb], "--help") == 0) {
			parse_display_help(argv[0], XY);
			return -1;
		}
		if ((strcmp(argv[nb], "--sensivity") == 0 || strcmp(argv[nb], "-s") == 0) && nb+1<argc) {
			myCfg->scope.SensV = atof(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "--pitch") == 0 || strcmp(argv[nb], "-pi") == 0) && nb+1<argc) {
			myCfg->scope.pitch = atof(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "-p")==0 || strcmp(argv[nb], "--phosphore")==0) && nb+1<argc) {
			myCfg->scope.phosphore = atoi(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "-r")==0 || strcmp(argv[nb], "--rate")==0) && nb+1<argc) {
			myCfg->scope.fps = atof(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "-z")==0 || strcmp(argv[nb], "--z-axis")==0) && nb<argc) {
			myCfg->scope.z = 1;
			nb++;
			continue;
		}
		syslog(LOG_ERR, "Switch %s unknown ! try -h or --help.\n", argv[nb]);
		return -2;
	}
	return 0;
}

/**
 * Parse arguments for oscilloscope XY 3D
 * @param nb The current argument who was being parsed
 * @param argc The number of arguments
 * @param argv The arguments
 * @param myCfg The config_t struct
 * @return 0: everything is ok
 *        -1: must exit
 *        -2: something bad happened
 */
int parse_scope_xy3d(int nb, int argc, char ** argv, config_t * myCfg)
{
	myCfg->scope.SensH = 1.f;
	while (nb<argc) {
		if (strcmp(argv[nb], "-h") == 0 || strcmp(argv[nb], "--help") == 0) {
			parse_display_help(argv[0], XY3D);
			return -1;
		}
		if ((strcmp(argv[nb], "-p")==0 || strcmp(argv[nb], "--phosphore")==0) && nb+1<argc) {
			myCfg->scope.phosphore = atoi(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "--pitch") == 0 || strcmp(argv[nb], "-pi") == 0) && nb+1<argc) {
			myCfg->scope.pitch = atof(argv[nb+1]);
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "--z-step") == 0 || strcmp(argv[nb], "-z") == 0) && nb+1<argc) {
			myCfg->scope.SensH = atof(argv[nb+1]);
			myCfg->scope.SensH_pow = 0;
			float f = myCfg->scope.SensH;
			while (f<1.f) {
				f*=10.f;
				myCfg->scope.SensH_pow--;
			}
			while (f>=10.f) {
				f*=0.1f;
				myCfg->scope.SensH_pow++;
			}
			myCfg->scope.SensH_abs = f;
			nb+=2;
			continue;
		}
		if ((strcmp(argv[nb], "-r")==0 || strcmp(argv[nb], "--rate")==0) && nb+1<argc) {
			myCfg->scope.fps = atof(argv[nb+1]);
			nb+=2;
			continue;
		}
		if (strcmp(argv[nb], "-s")==0 || strcmp(argv[nb], "--synchro")==0) {
			myCfg->scope.fillscreen = 1;
			nb++;
			continue;
		}
		syslog(LOG_ERR, "Switch %s unknown ! try -h or --help.\n", argv[nb]);
		return -2;
	}
	return 0;
}
/**
 * @brief parse_input Parse input options
 * @param nb The current option (needs to be input&output)
 * @param argc Number of total arguments
 * @param argv Arguments' list
 * @param myCfg The config to set
 * @return 0: OK, let's continue
 *        -1: oops, unknown option :(
 */
int parse_input(int * nb, int argc, char ** argv, config_t * myCfg)
{
	int retcode = -1;
	(*nb)++;
	if (strcmp(argv[*nb], "-")==0 || strcmp(argv[*nb], "stdin")==0) {
		syslog(LOG_INFO, "Standard input selected\n");

		myCfg->input.type = type_stdin;
		retcode = 0;
	} else if (strcmp(argv[*nb], "file")==0 && (*nb)+1<argc) {
		myCfg->input.type = type_file;
		myCfg->input.filename = argv[(*nb)+1];
		syslog(LOG_INFO, "file %s selected\n", myCfg->input.filename);
		(*nb)++;
		retcode = 0;
	} else if (strcmp(argv[*nb], "alsa")==0 && (*nb)+1<argc) {
		myCfg->input.type = type_alsa;
		myCfg->input.alsa.device = argv[(*nb)+1];
		syslog(LOG_INFO, "alsa device %s selected\n", myCfg->input.alsa.device);
		(*nb)++;
		retcode = 0;
	} else if (strcmp(argv[*nb], "jackd")==0) {
		syslog(LOG_INFO, "jackd input selected, please connect the port(s) manually\n");
		myCfg->input.type = type_jackd;
		retcode = 0;
	}

	return retcode;
}
