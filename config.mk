CC=gcc
CFLAGS=-s -O2 -Wall -I/home/adrien/Documents/Projets/C/visound/include \
	   -DVERSION='"0.2.0"' \
	   -DMAINTAINER='"Art SoftWare"' \
	   -DCONTRIBUTORS='"- Adrien Sohier\n- Matthias Devlamynck"' \
	   -DSUPPORTMAIL='"support@art-software.fr"' \
	   -DSDL20
LDFLAGS=-lm -lSDL2 -lasound -ljack
