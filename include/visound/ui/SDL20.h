/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#ifndef __SDL20_h__
#define __SDL20_h__


#ifdef SDL20

#include <sys/types.h>
#include <SDL2/SDL.h>

#include <visound/core/config.h>

/**
 * Use SDL v2.0 for graphical basis
 * @author Adrien Sohier
 */

/**
 * SDL2 Interface−related data
 */
typedef struct ui_t {
	SDL_Window * window;
	SDL_Renderer * render;
	SDL_Event event;
	SDL_Rect rect;
	SDL_Surface * surface;

	u_int32_t bgcolor;

	unsigned int width;
	unsigned int height;
	int weight;
} ui_t;

ui_t * ui_init(unsigned int width, unsigned int height);
void ui_free(ui_t * data);

void ui_drawpoint(unsigned int x, unsigned int y, u_int32_t color, ui_t * data);
void ui_drawline(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, u_int32_t color, ui_t * data);
void ui_refresh(config_t * myCfg, ui_t * data);
int ui_getEvent(ui_t * data);
void ui_set_bgcolor(ui_t * data, u_int32_t color);
void ui_set_coef(ui_t* data, unsigned int coef);

void ui_print_version();

int ui_saveToFile(ui_t * data, char * filename);

#endif


#endif
