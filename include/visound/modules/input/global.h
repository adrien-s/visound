/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

/**
 * **** Usage ****
 * - Run input_init at the beginning, specifying what backend you want
 * - Run input_read_sample each loop to get next sample in the buffer
 *   (the buffer will automatic fill itself if there is no sample to
 *    read anymore)
 * - Run input_clean at the end to release the structure's memory
 */

#ifndef __global_input_h__
#define __global_input_h__

#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>

#include <visound/modules/input/bufferStruct.h>
#include <visound/core/config.h>

input_t * input_init(u_int32_t samplesPerCycle, config_t * myCfg);
void input_clean(input_t * i);


#endif
