/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#ifndef __jackdinput_h__
#define __jackdinput_h__

#include <stdio.h>
#include <stdlib.h>
#include <jack/jack.h>

#include <visound/core/config.h>
#include <visound/modules/input/global.h>

/**
 * File input module
 * @author Adrien SOHIER
 */

void jackd_initBuf(input_t * in, config_t * myCfg);		// Inits the function inside the buffer structure
void jackd_fillBuffer(input_t * in);		// Fills the buffer with data
int  jackd_callback(jack_nframes_t nframes, void * arg);	// JackD callback
void jackd_clean(input_t * in);

/***** Internal functions *****/
void jackd_in_shutdown(void * arg);
void jackd_in_restart(void * arg);
int jackd_in_start(input_t * in);

#endif
