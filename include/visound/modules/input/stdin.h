/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#ifndef __stdin_h__
#define __stdin_h__

#include <visound/modules/input/global.h>

/**
 * Standard input module
 * @author Adrien SOHIER
 */

void stdin_initBuf(input_t * in);		// Inits the function inside the buffer structure
void stdin_fillBuffer(input_t * in);		// Fills the buffer with data

#endif
