/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#ifndef __bufstruct_h__
#define __bufstruct_h__

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <jack/jack.h>

/**
 * Buffer general structure for input
 * @author Adrien Sohier
 */

typedef struct input_t {
	struct bufStruct_t {
		void (*fillRoutine)(struct input_t *);	// The function to call to fill the buffer
		float * samples;					// Array of samples
		u_int32_t bufferSize;				// Number of samples in the buffer
		u_int32_t samplesToRead;			// Number of samples which haven't been read yet (when = 0, it is time to fill the buffer)
	} buffer;

	FILE * file;

	struct alsa_t {	// Data for ALSA backend
		snd_pcm_uframes_t frames;
		snd_pcm_t * handle;
		snd_pcm_stream_t stream;
		snd_pcm_hw_params_t * hwparams;
		char * pcm_name;
	} alsa;

	struct jackd_t { // Data for JackD backend
		jack_client_t * client;	// Client ptr
		jack_port_t ** input_ports;	// inputs ptr
		jack_default_audio_sample_t * waitbuffer; // buffer to be picked up by fillroutine internal callback

		u_int32_t singlebuffer_size;	// Size of the buffer for 1 port
		u_int32_t waitbuffer_size;		// size of the buffer
		u_int32_t waitbuffer_offset;	// offset of current next byte to read (0 ≤ offset ≤ waitbuffer_size)
		u_int32_t nb_ports;
	} jackd;
} input_t;

input_t * buffer_init(u_int32_t samplesPerCycle);	// Inits the buffer (not the callback function !)
float buffer_read_sample(input_t * in);				// Reads a sample. when no sample left, will call the fillRoutine if not null
void buffer_clean(input_t * in);						// Cleans up the structure after usage
float buffer_avgValue(input_t * in);					// Computes the average value of the buffer

#endif
