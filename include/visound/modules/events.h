/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#ifndef __events_h__
#define __events_h__

#include <syslog.h>

#include <visound/core/config.h>
#include <visound/modules/input/global.h>
#include <visound/modules/backend/events_SDL20.h>

/***** Actions *****/

/* Oscilloscope */
void evt_osc_sensh_up(config_t * myCfg);
void evt_osc_sensh_down(config_t * myCfg);

void evt_osc_sensv_up(config_t * myCfg);
void evt_osc_sensv_down(config_t * myCfg);

void evt_osc_ghosting_up(config_t * myCfg);
void evt_osc_ghosting_down(config_t * myCfg);

void evt_osc_switch_fill(config_t * myCfg);

void evt_osc_avg(config_t * myCfg, input_t * in);

void evt_osc_pitch_up(config_t * myCfg);
void evt_osc_pitch_down(config_t * myCfg);

/* Scan */
void evt_scan_ampli_up(config_t * myCfg);
void evt_scan_ampli_down(config_t * myCfg);

void evt_scan_eat_column(input_t * in);
void evt_scan_eat_line(config_t * myCfg, input_t * in);

void evt_scan_avg(config_t * myCfg, input_t * in);

/* Others */
void evt_pause_toggle(config_t * myCfg);

float evt_global_avg(input_t * in);

void evt_toggle_save(config_t * myCfg);
#endif /* modules/events.h */
