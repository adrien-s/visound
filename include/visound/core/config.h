/* ViSound ­ Modular Sound Visualizer
 * Copyright (C) 2013-2014, Art SoftWare
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 * ********************************************************************
 * For any questions, feture request or bug reports please contact me
 * at support@art-software.fr
 */

#ifndef __config_h__
#define __config_h__

#include <sys/types.h>

#define XY3D_Z_MAX 700.f

/**
 * Config storage structure
 * @author Adrien SOHIER
 */

typedef struct config_t {
	enum { SCANNER=0x01, SCOPE, SCOPEXY, SCOPEXY3D } mode;
	enum { type_stdin=0x01, type_file, type_alsa, type_jackd } input_type;
	float realsize;				// zoom
	unsigned int rate;			// audio rate (eg 44100 Hz / 192000 Hz / whatever… )
	unsigned int channels;		// number of channels (number of samples/second = rate*channels)
	unsigned char paused;
	char * outputDirName;		// Output dir for frames;
	unsigned char saving;		// Saving frames ?
	unsigned int current_frame;	// frame

	struct input {				// Input informations
		unsigned char type;		// The selected input type
		char * filename;
		struct alsa {
			char * device;
		} alsa;
	} input;

	struct scan {
		unsigned int coef;
		float ampli;
		float FreqH;			// Scanned lines per second ( ↔ height * FPS )
		float FreqV;			// Scanned images per second ( ↔ FPS )
		float deltaH;			// Pixel parts not displayed on a line
		float deltaV;			// pixels not displayed after last line
		struct real {			// Real screen size
			float width;
			float height;
		} real;
		struct used {			// Displayable screen size
			unsigned int width;
			unsigned int height;
		} used;
		unsigned int subchannels;	// Channels to display (independent of real channel count)
		struct subcolors {
			float * red;
			float * green;
			float * blue;
		} subcolors;
		unsigned int x;
		unsigned int y;
	} scan;

	struct scope {
		unsigned char fillscreen;
		unsigned char phosphore;
		unsigned char z;		// Use Z axis in XY mode?
		float fps;
		float pitch;			// Phosphore pitch scale
		unsigned int width;
		unsigned int height;
		int weight;

		float SensV;			// unite / div (0.25 ⇒ when input=1.0 will be at the top)
		float SensH;			// seconds / div (0.1 ⇒ 1 second to scan screen from left to right)
		int SensH_pow;
		int SensH_abs;

		unsigned int refresh_count;	// number of samples to count for screen refresh, = rate / 30
		unsigned int channels_count;	// number of channels

		float offset_v;			// calculated, = (height*0.5f) in mono channel mode

		float CoefV;			// calculated, = (height*0.125f) / SensV
		float CoefH;			// calculated, = (width*0.1f) / (rate * SensH)

		float x;			// x position, 0 ≤ x ≤ width ( x += CoefH each collected sample)
		float y;			// y position, calculated ( = offset_v + (input*CoefV)
		struct trigger {	// trigger sync config
			unsigned int channel;
			float level;
			float precision;
		} trigger;
	} scope;

	struct xy3D {
		int curIdx;
		int lastIdx;
		u_int32_t bufSize;
		float Cz;

		struct buffer {
			float *x;
			float *y;
		} buffer;
	} xy3D;

} config_t;

#endif
