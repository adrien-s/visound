#!/usr/bin/env bash
PV=$(git describe)
PN=visound

install -dm755 ./${PN}:${PV}

make DESTDIR="$(dirname "${0}")/${PN}:${PV}" install

tar -cvpf ./${PN}:${PV}.stow.tar ./${PN}:${PV}
xz -ze9vT 4 ./${PN}:${PV}.stow.tar
rm -r ./${PN}:${PV}
